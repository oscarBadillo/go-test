package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

type Item struct {
	Id string `json:"Id"`
}

func main() {
	sess := session.Must(
		session.NewSessionWithOptions(
			session.Options{
				SharedConfigState: session.SharedConfigEnable,
			}))
	sess, err := session.NewSession(&aws.Config{Region: aws.String("us-east-1")})
	if err != nil {
		fmt.Println("There was an error statting a new session")
	}

	svc := dynamodb.New(sess)

	uuidWithHyphen := uuid.New()
	fmt.Println(uuidWithHyphen)
	uuid := strings.Replace(uuidWithHyphen.String(), "-", "", -1)

	item := Item{Id: uuid}

	AddDBItem(item, svc)

	GetDBItem(svc, uuid)
}

func AddDBItem(item Item, svc *dynamodb.DynamoDB) {
	av, err := dynamodbattribute.MarshalMap(item)
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("MyTable"),
	}
	_, err = svc.PutItem(input)
	if err != nil {
		fmt.Println("Got error calling PutItem")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	fmt.Println("Successfully added item to table!")
}

func GetDBItem(svc *dynamodb.DynamoDB, uuid string) {
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("MyTable"),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String(uuid),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	item := Item{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)

	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if item.Id == "" {
		fmt.Println("Could not find the id")
		return
	}

	fmt.Println("Found item:")
	fmt.Println(item.Id)
}
