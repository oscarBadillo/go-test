module bitbucket.org/oscarBadillo/go-test

go 1.17

require (
	github.com/aws/aws-sdk-go v1.42.12 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)
